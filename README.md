## How to run the project

In the project directory, run:

`yarn install` or `npm install`

Then run:
`yarn start` or `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Available views

Home: [http://localhost:3000](http://localhost:3000)

Login: [http://localhost:3000/login](http://localhost:3000/login)

Exhibition Preview: [http://localhost:3000/exhibitions](http://localhost:3000/exhibitions)

Plan Your Visit: [http://localhost:3000/plan-your-visit](http://localhost:3000/plan-your-visit)

Collections: [http://localhost:3000/collections](http://localhost:3000/collections)
