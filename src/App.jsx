import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { createGlobalStyle } from "styled-components";

import { LoginPage, Home, PlanYourVisit, Collections, Exhibitions } from "pages";
import { Container, Header } from "components";

const GlobalStyle = createGlobalStyle`
  body {
    font-family: 'Montserrat', sans-serif;
		overflow-x: hidden;
		max-width: 600px;
  }

  * {
    box-sizing: border-box;
  }

	.page-container {
		transition: transform 0.3s ease;
		transform: translateX(0);

		.active-menu + & {
			transform: translateX(calc(100% - 70px))
		}
	}
`;

const titles = {
	"/": "Home",
	"/login": "Log In",
	"/plan-your-visit": "Plan Your Visit",
	"/collections": "Collections",
	"/exhibitions": "Exhibition Preview",
};

const App = () => {
	return (
		<>
			<GlobalStyle />
			<Router>
				<Route path="/" render={routeProps => <Header {...routeProps} titles={titles} />} />
				<Container className="page-container">
					<Switch>
						<Route exact path="/" component={Home} />
						<Route path="/login" component={LoginPage} />
						<Route path="/plan-your-visit" component={PlanYourVisit} />
						<Route path="/collections" component={Collections} />
						<Route path="/exhibitions" component={Exhibitions} />
					</Switch>
				</Container>
			</Router>
		</>
	);
};

export default App;
