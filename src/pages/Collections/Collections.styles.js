import styled from "styled-components";
import { InputText, Icon } from "components";
import { vars } from "utils";

export const StyledInput = styled(InputText)`
	background: ${vars.white};
	border: solid ${vars.primary} 2px;
	color: ${vars.primary};
	font-size: ${props => (props.size ? `${props.size / 16}rem` : "0.625rem")};
	padding: 0.5em;
	width: 100%;
	padding-right: 40px;

	&::placeholder {
		color: ${vars.primary};
	}
`;

export const ExploreWrapper = styled.div`
	position: relative;
	margin-top: 1em;
`;
export const StyledIcon = styled(Icon)`
	position: absolute;
	right: 0;
	top: 50%;
	transform: translateY(-50%);
	pointer-events: none;
`;

export const CollectionsWrapper = styled.div`
	display: flex;
	flex-wrap: wrap;
	margin-top: 1em;

	div {
		flex: 1 1 50%;
		padding: 0 5px;
		width: 50%;
		margin-bottom: 0.75em;
	}
`;

export const Image = styled.img`
	width: 100%;
	height: auto;
`;
