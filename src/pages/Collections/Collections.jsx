import React from "react";
import * as S from "./Collections.styles";
import { Text } from "components";
import greek from "images/collections/greek.jpg";
import american from "images/collections/american.jpg";
import cubism from "images/collections/cubism.jpg";
import decorative from "images/collections/decorative.jpg";
import deStijl from "images/collections/deStijl.jpg";
import impressionism from "images/collections/impressionism.jpg";

const collectionsData = [
	{ image: decorative, title: "Decorative Arts & Crafts" },
	{ image: impressionism, title: "American Impressionism" },
	{ image: deStijl, title: "De Stijl" },
	{ image: cubism, title: "Cubism" },
	{ image: american, title: "American" },
	{ image: greek, title: "Greek Antiquites" },
];

export const Collections = () => {
	return (
		<>
			<S.ExploreWrapper>
				<S.StyledIcon type="searchOrange" />
				<S.StyledInput size="18" placeholder="Explore the Collection" />
			</S.ExploreWrapper>
			<Text color="primary" size="12" style={{ textDecoration: "underline" }}>
				Advanced Search
			</Text>
			<S.CollectionsWrapper>
				{collectionsData.map(({ image, title }) => (
					<div>
						<S.Image src={image} alt={title} />
						<Text size="12" color="primary" textTransform="uppercase">
							{title}
						</Text>
					</div>
				))}
			</S.CollectionsWrapper>
		</>
	);
};
