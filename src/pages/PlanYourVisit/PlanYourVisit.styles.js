import styled from "styled-components";
import { vars } from "utils";

export const DateHeaderWrapper = styled.div`
	display: flex;
	justify-content: space-between;
	margin: 2em 0 1em;
	padding-bottom: 5px;
	position: relative;

	&::after {
		content: "";
		position: absolute;
		left: 0;
		right: 0;
		bottom: 0;
		height: 1px;
		background-color: ${vars.grayLight};
		box-shadow: ${vars.boxShadow};
		z-index: -1;
	}

	button {
		transition: transform 0.3s ease-out;
		position: relative;

		&.active {
			transform: translateY(-10px);
			text-decoration: underline;
			&::after {
				opacity: 1;
			}
		}

		&::after {
			content: "";
			position: absolute;
			bottom: -16px;
			left: 0;
			height: 3px;
			background-color: ${vars.primary};
			width: 100%;
			opacity: 0;
			z-index: 2;
		}
	}
`;

export const DateWrapper = styled.div`
	display: flex;
	justify-content: space-between;

	span {
		display: none;
		text-align: center;
		white-space: nowrap;
		width: 100%;

		&.active {
			display: block;
		}
	}
`;
