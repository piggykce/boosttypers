import React, { useState } from "react";
import * as S from "./PlanYourVisit.styles";
import { Text, Basket } from "components";

export const PlanYourVisit = () => {
	const [activeIndex, setActiveIndex] = useState(1);

	const dates = ["Today", "Tomorrow", "Other"];
	const handleDateClick = index => setActiveIndex(index);

	return (
		<>
			<Text color="black" textShadow size="24" as="h3" style={{ fontWeight: 400, margin: "1em 0 0" }}>
				Skip the Line. <br />
				Purchase Tickets.
			</Text>
			<Text color="primary" size="12" textShadow style={{ paddingRight: "50px" }}>
				All exhibitions, audio tours, and films included in the price of admission.
			</Text>
			<S.DateHeaderWrapper>
				{dates.map((date, index) => (
					<Text
						as="button"
						size="18"
						color={index === activeIndex ? "black" : "grayText"}
						className={index === activeIndex ? "active" : undefined}
						key={index}
						textShadow
						onClick={() => handleDateClick(index)}
					>
						{date}
					</Text>
				))}
			</S.DateHeaderWrapper>
			<S.DateWrapper>
				{dates.map((date, index) => (
					<Text size="12" color="black" className={index === activeIndex ? "active" : undefined} key={index} textShadow>
						March {new Date().getDate() + index}, 2016 <br />
						Open 10:30am-5:30pm
					</Text>
				))}
			</S.DateWrapper>
			<Basket />
		</>
	);
};
