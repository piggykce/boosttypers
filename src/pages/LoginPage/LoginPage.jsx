import React, { useEffect } from "react";
import * as S from "./LoginPage.styles";
import { Title, Text, InputText, Button } from "components";
import { Link } from "react-router-dom";

export const LoginPage = () => {
	useEffect(() => {
		document.title = "Login Page";
	});

	return (
		<S.Background>
			<Title xxl>
				Your <br />
				Art <br />
				Museum
			</Title>
			<Text size="10" style={{ margin: "0 0 1em" }}>
				151 3rd St <br />
				San Franciso, CA 94103
			</Text>
			<InputText placeholder="Email address" type="email" />
			<InputText placeholder="Password" type="password" />
			<Text size="10" style={{ textAlign: "right", margin: "0.5em 0 2em" }}>
				Forgot your password?
			</Text>
			<Button as={Link} to="/" size="10">
				Log In
			</Button>
			<Text size="10">Don't have an account?</Text>
		</S.Background>
	);
};
