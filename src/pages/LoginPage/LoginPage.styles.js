import styled from "styled-components";
import img from "images/main.png";

export const Background = styled.div`
	width: 100%;
	min-height: 100vh;
	display: flex;
	flex-direction: column;
	justify-content: center;
	padding: 30px;

	&::before {
		content: "";
		position: absolute;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		z-index: -1;
		background: url(${img});
		background-size: cover;
		background-position: 100% 50%;
		filter: blur(2px);
		transform: scale(1.05);
	}
`;
