import styled from "styled-components";
import { Text } from "components";
import { vars } from "utils";

export const BiographyTextWrapper = styled.div`
	position: relative;
	margin: 1em 0;

	svg {
		position: absolute;
		right: 0;
		top: calc(50% - 5px);
		transition: transform 0.2s ease;
		transform: rotate(90deg);
	}
	&.active svg {
		transform: rotate(0);
	}
`;

export const BiographyDescription = styled(Text)`
	opacity: 0;
	visibility: hidden;
	max-height: 0;
	transition: all 0.3s ease;

	&.active {
		max-height: 500px;
		opacity: 1;
		visibility: visible;
	}
`;

export const CarouselWrapper = styled.div`
	display: flex;
	flex-direction: column;
	text-align: center;
	padding: 1em;
	position: relative;
	overflow: hidden;

	img {
		width: 100%;
		height: auto;
		box-shadow: ${vars.boxShadow};
	}

	.another {
		position: absolute;
    left: 96%;
    top: 0;
    transform: scale(0.7);
    z-index: -1;
    transform-origin: 0 50%;
}


	}
`;

export const DotNav = styled.div`
	display: flex;
	justify-content: center;
	margin: 0.5em 0;

	span {
		width: 6px;
		height: 6px;
		border-radius: 50%;
		background: ${vars.gray};
		margin: 0 1px;
		display: inline-block;

		&.active {
			background: ${vars.primary};
		}
	}
`;
