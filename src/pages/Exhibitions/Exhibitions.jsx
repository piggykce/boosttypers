import React, { useState } from "react";
import * as S from "./Exhibitions.styles";
import { Text, Title } from "components";
import exhibition from "images/exhibition.png";

export const Exhibitions = () => {
	const [biographyVisibility, setBiographyVisibility] = useState(false);

	const handleBiography = () => setBiographyVisibility(!biographyVisibility);
	return (
		<>
			<Text size="10" color="grayText" textTransform="uppercase" style={{ marginTop: "2em" }}>
				Retroperspective
			</Text>
			<Title big color="black" style={{ margin: "0.5em 0" }}>
				DOROTHEA LANGE
			</Title>
			<Text color="primary" style={{ display: "block", marginTop: "1em" }}>
				OCTOBER 15 - MARCH 18
			</Text>
			<Text size="10" color="grayText">
				FLOOR 3
			</Text>
			<S.CarouselWrapper>
				<img src={exhibition} alt="Exhibition Preview" />
				<img src={exhibition} className="another" alt="Exhibition Preview" />
				<S.DotNav>
					<span></span>
					<span className="active"></span>
					<span></span>
					<span></span>
					<span></span>
				</S.DotNav>
				<Text size="8" color="grayText" style={{ margin: "1em 0" }}>
					“Abandoned Dust Bowl Home” <br />
					Gelatin silver print <br />
					about 1935 - 1940
				</Text>
			</S.CarouselWrapper>
			<S.BiographyTextWrapper className={biographyVisibility ? "active" : undefined} onClick={handleBiography}>
				<Text color="grayText" size="10" textTransform="uppercase">
					Biography
				</Text>
				<svg width="15" height="9" viewBox="0 0 15 9" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M13.5 1.5L7.5 7.5L1.5 1.5" stroke="#AAAAAA" strokeWidth="2" strokeLinecap="square" />
				</svg>
			</S.BiographyTextWrapper>
			<S.BiographyDescription className={biographyVisibility ? "active" : undefined} color="grayText" size="10">
				Dorothea Lange (May 26, 1895 – October 11, 1965) was an influential American documentary photographer and
				photojournalist, best known for her Depression-era work for the Farm Security Administration (FSA). Lange's
				photographs humanized the consequences of the Great Depression and influenced the development of documentary
				photography. Born of second generation German immigrants on May 26, 1895, at 1041 Bloomfield Street, Hoboken,
				New Jersey,[1][2] Dorothea Lange was named Dorothea Margaretta Nutzhorn at birth. She dropped her middle name
				and assumed her mother's maiden name after her father abandoned the family when she was 12 years old, one of two
				traumatic incidents early in her life. The other was her contraction of polio at age seven which left her with a
				weakened right leg and a permanent limp.[1][2] "It formed me, guided me, instructed me, helped me and humiliated
				me," Lange once said of her altered gait. "I've never gotten over it, and I am aware of the force and power of
				it."[3]
			</S.BiographyDescription>
		</>
	);
};
