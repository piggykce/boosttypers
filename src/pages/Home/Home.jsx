import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import img from "images/main.png";
import { Text, Title, Button, Icon } from "components";

const Image = styled.img`
	margin-left: calc(-1 / 2 * (100vw - 100%));
	width: 100vw;
`;

const ButtonWrapper = styled.div`
	width: 100%;
	padding: 2em;
	display: flex;
`;

const InfoWrapper = styled.div`
	display: flex;
	justify-content: space-between;
	margin-left: calc(-1 / 2 * (100vw - 100%));
	padding: 15px;

	.single-info {
		display: flex;
		flex-wrap: wrap;
		align-items: center;
		white-space: nowrap;

		img {
			box-sizing: content-box;
			width: 20px;
			height: auto;
			transform: translateY(-5px);
		}
	}
`;

export const Home = () => {
	return (
		<>
			<Image src={img} alt="Home image" />
			<Text size="10" color="grayText" textTransform="uppercase" style={{ marginTop: "2em", display: "inline-block" }}>
				Exhibition
			</Text>
			<Title big color="black" style={{ margin: "0.25em 0" }}>
				Masters <br />
				Old And <br />
				New
			</Title>
			<Title as="h2" color="primary" style={{ margin: "0.25em 0" }}>
				April 15 - September 20
			</Title>
			<Text size="10" color="grayText" textTransform="uppercase">
				Floor 5
			</Text>
			<ButtonWrapper>
				<Button as={Link} to="plan-your-visit" style={{ width: "100%" }}>
					Plan Your Visit
				</Button>
			</ButtonWrapper>
			<InfoWrapper>
				<div className="single-info">
					<Icon type="localization" noWidth />
					<Text color="primary" size="10">
						151 3rd St <br />
						San Francisco, CA 94103
					</Text>
				</div>
				<div className="single-info">
					<Icon type="clock" noWidth />
					<Text color="primary" size="10">
						Open today <br />
						10:00am - 5:30pm
					</Text>
				</div>
			</InfoWrapper>
		</>
	);
};
