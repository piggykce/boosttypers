export { LoginPage } from "./LoginPage/LoginPage";
export { Home } from "./Home/Home";
export { PlanYourVisit } from "./PlanYourVisit/PlanYourVisit";
export { Collections } from "./Collections/Collections";
export { Exhibitions } from "./Exhibitions/Exhibitions";
