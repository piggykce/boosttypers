import React from "react";
import styled from "styled-components";
import { vars } from "utils";

const StyledText = styled.span`
	display: inline-block;
	color: ${props => (props.color ? vars[props.color] : vars.white)};
	font-size: ${props => (props.size ? `${props.size / 16}rem` : "0.625rem")};
	text-decoration: none;
	text-transform: ${props => props.textTransform || "none"};
	text-shadow: ${props => (props.textShadow ? vars.boxShadow : "none")};
`;

export const Text = ({ children, as = "span", ...props }) => {
	return (
		<StyledText as={as} {...props}>
			{children}
		</StyledText>
	);
};
