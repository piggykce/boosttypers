import React from "react";
import styled from "styled-components";
import { vars } from "utils";

const StyledButton = styled.button`
	color: ${props => props.color || vars.white};
	background-color: ${vars.primary};
	text-align: center;
	border: none;
	padding: 1em;
	margin: 1em 0;
	text-decoration: none;
	cursor: pointer;
	box-shadow: ${vars.boxShadow};
	font-size: ${props => (props.size ? `${props.size / 16}rem` : "0.625rem")};
`;

export const Button = ({ children, ...props }) => {
	return <StyledButton {...props}>{children}</StyledButton>;
};
