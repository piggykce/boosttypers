import React from "react";
import Helmet from "react-helmet";

export const TitleComponent = ({ title = "The Art Museum" }) => {
	return (
		<Helmet>
			<title>{title}</title>
		</Helmet>
	);
};
