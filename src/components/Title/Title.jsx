import React from "react";
import styled from "styled-components";
import { vars } from "utils";

const StyledTitle = styled.h1`
	color: ${props => vars[props.color] || vars.white};
	font-size: ${props => {
		if (props.xxl) return "2rem";
		if (props.big) return "1.5rem";
		return "1.125rem";
	}};
	text-transform: uppercase;
`;

export const Title = ({ children, as = "h1", ...props }) => {
	return (
		<StyledTitle as={as} {...props}>
			{children}
		</StyledTitle>
	);
};
