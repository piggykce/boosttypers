import React from "react";
import styled from "styled-components";
import { vars } from "utils";

const StyledInput = styled.input`
	color: ${vars.black};
	padding: 1em;
	margin: 0.15em 0;
	border-radius: 0;

	&::placeholder {
		color: ${vars.black};
	}
`;

export const InputText = props => {
	return <StyledInput {...props} type={props.type || "text"} />;
};
