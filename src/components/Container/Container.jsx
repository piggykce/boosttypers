import React from "react";
import styled from "styled-components";

const StyledContainer = styled.main`
	max-width: 960px;
	padding: 0 20px;
`;

export const Container = ({ children, ...props }) => {
	return <StyledContainer {...props}>{children}</StyledContainer>;
};
