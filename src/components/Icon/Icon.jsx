import React from "react";
import styled from "styled-components";
import search from "images/icons/search.svg";
import searchOrange from "images/icons/searchOrange.svg";
import events from "images/icons/events.svg";
import artists from "images/icons/artists.svg";
import collections from "images/icons/collections.svg";
import member from "images/icons/member.svg";
import shop from "images/icons/shop.svg";
import visit from "images/icons/visit.svg";
import clock from "images/icons/clock.svg";
import localization from "images/icons/localization.svg";

const StyledIcon = styled.img`
	max-height: 50px;
	padding: 0 0.5em;
`;

const IconWrapper = styled.div`
	width: ${props => (props.noWidth ? "" : "60px")};
	flex-basis: ${props => (props.noWidth ? "" : "1 1 60px")};
	text-align: center;
`;

const icons = {
	search,
	events,
	artists,
	collections,
	member,
	shop,
	visit,
	clock,
	localization,
	searchOrange,
};

export const Icon = ({ type, noWidth, ...props }) => {
	return (
		<IconWrapper noWidth={noWidth}>
			<StyledIcon src={icons[type]} alt={`${type} icon`} {...props} />
		</IconWrapper>
	);
};
