import styled from "styled-components";
import { Container, InputText } from "components";
import { vars } from "utils";

export const StyledContainer = styled(Container)`
	display: flex;
	justify-content: space-between;
	align-items: center;
	height: 60px;
	width: 100%;
`;

export const Navigation = styled.nav`
	position: absolute;
	top: 0;
	bottom: 0;
	left: 0;
	right: 70px;
	background: ${vars.primary};
	box-shadow: ${vars.boxShadow};
	padding: 2em 2em 2em 1em;
	transition: transform 0.3s ease;
	transform: translateX(-110%);

	&.visible {
		transform: translateX(0);
	}

	ul {
		padding: 0;
		margin: 0;
	}
	li {
		list-style: none;
		display: flex;
		align-items: center;
		padding: 0.75em 0;

		a {
			flex: 1;
		}
	}
`;

export const StyledInput = styled(InputText)`
	background: ${vars.primary};
	border: solid ${vars.white} 2px;
	color: ${vars.white};
	font-size: ${props => (props.size ? `${props.size / 16}rem` : "0.625rem")};
	padding: 0.5em;
	width: 100%;

	&::placeholder {
		color: ${vars.white};
	}
`;

export const HamburgerMenu = styled.button`
	background: none;
	border: none;
`;
