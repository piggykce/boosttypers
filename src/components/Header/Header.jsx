import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Text, Title, Icon } from "components";
import * as S from "./Header.styles";
import hamburgerMenu from "images/hamburger.svg";

const nav = [
	{ name: "Exhibitions & Events", url: "/exhibitions", type: "events" },
	{ name: "Artists & Artworks", url: "#", type: "artists" },
	{ name: "Collections", url: "/collections", type: "collections" },
	{ name: "Plan Your Visit", url: "/plan-your-visit", type: "visit" },
	{ name: "Become a Member", url: "#", type: "member" },
	{ name: "Shop", url: "#", type: "shop" },
];
export const Header = ({ location, titles }) => {
	const [navStatus, setNavStatus] = useState(false);

	const noHeaderPages = ["/login"];
	const noTitlePages = ["/"];
	const { pathname } = location;

	const handleNavClick = () => setNavStatus(!navStatus);

	if (noHeaderPages.includes(pathname)) {
		return null;
	}

	return (
		<header className={navStatus ? "active-menu" : undefined}>
			<S.StyledContainer>
				<Text as={Link} to="/" color="black" size="10" style={{ textTransform: "uppercase" }}>
					The <br />
					Art <br />
					Museum
				</Text>
				<Title color="primary" style={{ textTransform: "none", fontSize: "1.125rem" }}>
					{!noTitlePages.includes(pathname) && titles[pathname]}
				</Title>

				<S.HamburgerMenu onClick={handleNavClick}>
					<img src={hamburgerMenu} alt="Menu button" />
				</S.HamburgerMenu>
			</S.StyledContainer>

			<S.Navigation className={navStatus ? "visible" : undefined}>
				<ul>
					<li>
						<Icon type="search" />
						<S.StyledInput size="18" placeholder="Search" />
					</li>
					{nav.map(({ name, url, type }) => (
						<li onClick={handleNavClick} key={name}>
							<Icon type={type} />
							<Text as={Link} to={url} size="18">
								{name}
							</Text>
						</li>
					))}
				</ul>
			</S.Navigation>
		</header>
	);
};
