import React from "react";
import styled from "styled-components";
import { vars } from "utils";
import { Text } from "components";

const StyledButton = styled.button`
	color: ${vars.graySecondary};
	display: inline-flex;
	justify-content: center;
	align-items: center;
	background: transparent;
	width: 18px;
	line-height: 14px;
	padding: 0 0 2px;
	border: solid 1px ${vars.graySecondary};
	text-align: center;
	margin: 0.5em;
	text-decoration: none;
	cursor: pointer;
	position: relative;

	&:focus {
		outline: none;
		background: ${vars.focusColor};
	}

	&::after {
		content: "";
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		border: solid 1px ${vars.graySecondary};
		filter: blur(1px);
		transform: translate(0, 2px);
	}
`;

export const BasketButton = ({ type, ...props }) => {
	return (
		<Text as={StyledButton} {...props} textShadow color="graySecondary" size="16">
			{type === "plus" ? "+" : "-"}
		</Text>
	);
};
