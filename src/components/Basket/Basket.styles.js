import styled from "styled-components";
import { vars } from "utils";
export const StyledBasket = styled.div`
	display: flex;
	flex-direction: column;
	margin-top: 1rem;
`;

export const Adults = styled.div`
	.adults-main {
		display: flex;
		justify-content: space-between;
	}
	.adults-senior-title {
		padding-left: 60px;
		margin-bottom: 0;
	}

	.adults-main-title {
		position: relative;

		&::after {
			content: "";
			position: absolute;
			left: 50%;
			top: 100%;
			transform: translate(2px, -15px);
			width: 1px;
			background: ${vars.black};
			height: 50px;
			box-shadow: ${vars.boxShadow};
		}
	}
`;

export const Students = styled.div`
	display: flex;
	justify-content: space-between;
	margin: 2em 0;
`;

export const Total = styled.div`
	display: flex;
	justify-content: space-between;
	padding: 0.5em 1em;
	position: relative;

	&::before {
		content: "";
		position: absolute;
		left: 0;
		top: 0;
		right: 0;
		box-shadow: ${vars.boxShadow};
		background: ${vars.primary};
		height: 2px;
	}
`;
