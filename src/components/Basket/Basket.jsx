import React, { useState, useEffect } from "react";
import * as S from "./Basket.styles";
import { Text, BasketButton, Button } from "components";

export const Basket = () => {
	const [total, setTotal] = useState(0);
	const [basket, setBasket] = useState({ student: 0, senior: 0, adult: 0 });

	const prices = {
		student: 5,
		senior: 5,
		adult: 8,
	};

	useEffect(() => {
		let total = 0;
		for (let [key, value] of Object.entries(basket)) {
			total += value * prices[key];
		}
		setTotal(total);
	}, [basket, prices]);

	const handleQuantity = ({ value, type }) => {
		if (basket[type] + value < 0) return;
		setBasket({ ...basket, [type]: basket[type] + value });
	};

	return (
		<S.StyledBasket>
			<S.Adults>
				<div className="adults-main" style={{ marginBottom: "0.5em" }}>
					<Text as="label" color="black" textShadow size="18" className="adults-main-title">
						Adults
					</Text>
					<div className="quantity">
						<BasketButton type="minus" onClick={() => handleQuantity({ value: -1, type: "adult" })} />
						<Text
							color={basket.adult > 0 ? "black" : "grayText"}
							textShadow
							size="18"
							style={{ width: "20px", textAlign: "center" }}
						>
							{basket.adult}
						</Text>
						<BasketButton type="plus" onClick={() => handleQuantity({ value: 1, type: "adult" })} />
					</div>
				</div>
				<div className="adults-main">
					<Text as="label" color="black" textShadow size="18" className="adults-senior-title">
						Seniors <br />
						<Text color="grayText" size="10">
							65+ with ID
						</Text>
					</Text>
					<div className="quantity" style={{ marginTop: "1em" }}>
						<BasketButton type="minus" onClick={() => handleQuantity({ value: -1, type: "senior" })} />
						<Text
							color={basket.senior > 0 ? "black" : "grayText"}
							textShadow
							size="18"
							style={{ width: "20px", textAlign: "center" }}
						>
							{basket.senior}
						</Text>
						<BasketButton type="plus" onClick={() => handleQuantity({ value: 1, type: "senior" })} />
					</div>
				</div>
			</S.Adults>

			<S.Students>
				<Text as="label" color="black" textShadow size="18" className="adults-student-title">
					Students <br />
					<Text color="grayText" size="10">
						with ID
					</Text>
				</Text>
				<div className="quantity">
					<BasketButton type="minus" onClick={() => handleQuantity({ value: -1, type: "student" })} />
					<Text
						color={basket.student > 0 ? "black" : "grayText"}
						textShadow
						size="18"
						style={{ width: "20px", textAlign: "center" }}
					>
						{basket.student}
					</Text>
					<BasketButton type="plus" onClick={() => handleQuantity({ value: 1, type: "student" })} />
				</div>
			</S.Students>
			<S.Total>
				<Text color="black" textShadow size="18">
					Total
				</Text>
				<Text color="black" textShadow size="18">
					${total}
				</Text>
			</S.Total>
			<Button style={{ margin: "2em 4em" }}>Continue to Payment</Button>
		</S.StyledBasket>
	);
};
