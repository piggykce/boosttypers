export const vars = {
	primary: "#FF473A",
	white: "#FFFFFF",
	black: "#000000",
	grayText: "#979797",
	gray: "#DADADA",
	graySecondary: "#D4D4D4",
	grayDark: "#AAAAAA",
	grayLight: "#F4F5F2",
	boxShadow: "0 4px 4px rgba(0, 0, 0, 0.25)",
	focusColor: "rgba(254, 98, 89, 0.16)",
};
